'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  OPEN_PROXY: true, // 是否开启代理, 重置后需重启vue-cli,
  FILE_HOST: '"https://127.0.0.1/uploads"',
  WEB_SOCKET_HOST: '"127.0.0.1"'
})
